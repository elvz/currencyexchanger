import { useState, useEffect } from "react";

function Header() {
  const [items, setItems] = useState([]);

  const getValues = async () => {
    const res = await fetch(
      "https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5"
    );
    const data = await res.json();
    setItems(data);
  };

  useEffect(() => {
    getValues();
  }, []);

  return (
      <div className="header">
          <h1>Currency Converter</h1>
          <ul className="list-items">
      {items.map((item) => (
        <li className="item" key={item.ccy}>
          {item.ccy} - {item.buy}/{item.sale}
        </li>
      ))}
    </ul>
      </div>
    
  );
}

export default Header;
